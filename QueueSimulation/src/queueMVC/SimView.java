package queueMVC;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.Toolkit;
import java.lang.reflect.Method;
import java.text.DecimalFormat;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.JTextArea;
import javax.swing.JTextField;


/**
 * The SimView Class displays the simulation and all accompanying data as well as allows for starting
 *  and pausing the controller via buttonListener
 * @author hudson
 *
 */
@SuppressWarnings("serial")
public class SimView extends JFrame
{
	//Constants
	private final int TELLER_WIDTH = 160;
	private final int TELLER_HEIGHT = 150;
	private final String TELLER1_IMG = "assets/Cashier1.png";
	private final String TELLER2_IMG = "assets/Cashier2.png";
	private final String TELLER3_IMG = "assets/Cashier3.png";
	private final String TELLER4_IMG = "assets/Cashier4.png";
	private final String TELLER5_IMG = "assets/Cashier5.png";
	private final String CUSTOMER1_IMG = "assets/Customer1.png";
	private final String CUSTOMER2_IMG = "assets/Customer2.png";
	private final String CUSTOMER3_IMG = "assets/Customer3.png";
	private final String CUSTOMER4_IMG = "assets/Customer4.png";
	private final String CUSTOMER5_IMG = "assets/Customer5.png";
	private final String CUSTOMER6_IMG = "assets/Customer6.png";
	private final String CUSTOMER7_IMG = "assets/Customer7.png";
	private final String CUSTOMER8_IMG = "assets/Customer8.png";
	private final String COUNTER_IMG = "assets/Register.png";
	private final int COUNTER_BOX_WIDTH = 20;
	private final int COUNTER_BOX_HEIGHT = 20;
	private final int CUSTOMER_WIDTH = 160;
	private final int CUSTOMER_HEIGHT = 150;
	private final int ROW_1 = 320;
	private final int ROW_2 = 460; 
	private final int MAX_PEOPLE_IN_LINE = 6;
	private final int MAX_NUM_OF_TELLERS = 5;

	//Data Members
	private Image[] myScaledCustomer;
	private Image myScaledCounter;
	private Image[] myScaledCashier;
	private SimController myController;
	private Container myContentPane;
	private JLabel [] myTotalServed;
	private ButtonListener myStartPauseListener;
	private JLabel [][] myCustomer;
	private JButton myStartPauseButton;
	private JLabel[] myCounter;
	private JLabel [] myTeller;
	private JPanel mySimPanel;
	private JPanel myToolbar;
	private JPanel mySidebar;
	private JTextArea myOverallDisplay;
	private JTextArea[] myCashierDisplays;
	private JPanel myOverSimPanel;
	
	private JTextField myGenerateTimeName = new JTextField("CUSTOMER GENERATION TIME: ");
	private JTextField myGenerateTimeField = new JTextField(null);
	private JTextField myNumCustomersName = new JTextField("TOTAL NUMBER OF CUSTOMERS: ");
	private JTextField myNumCustomersField = new JTextField(null);
	private JTextField myNumCashiersName = new JTextField("TOTAL NUMBER OF CASHIERS: ");
	private JSlider myNumCashiersSlider = new JSlider(JSlider.HORIZONTAL, 1, 5, 1);
	private JTextField myServiceTimeName = new JTextField("MAXIMUM SERVICE TIME: ");
	private JTextField myServiceTimeField = new JTextField(null);
	
	private Font myFont = new Font("Helvetica", Font.PLAIN, 12);
	private Font myFont2 = new Font("Helvetica", Font.PLAIN, 10);
	private Font myFont3 = new Font("Helvetica", Font.BOLD, 15);
	
	//Constructor
	
	/**
	 * Constructor that creates the view.
	 * 
	 * @param controller the SimulationController that gives function to the buttons.
	 * @author hudson
	 */
	public SimView(SimController controller)
	{
		
		Image customer = Toolkit.getDefaultToolkit().getImage(CUSTOMER1_IMG);
		myScaledCustomer = new Image[7];
		myScaledCustomer[0] = customer.getScaledInstance((int)(CUSTOMER_WIDTH * .85),(int)(CUSTOMER_HEIGHT*.85),Image.SCALE_FAST);
		customer = Toolkit.getDefaultToolkit().getImage(CUSTOMER2_IMG);
		myScaledCustomer[1] = customer.getScaledInstance((int)(CUSTOMER_WIDTH * .85),(int)(CUSTOMER_HEIGHT*.85),Image.SCALE_FAST);
		Image counter = Toolkit.getDefaultToolkit().getImage(COUNTER_IMG);
		
		
		
		
		
		
		myScaledCounter = counter.getScaledInstance((int)(CUSTOMER_WIDTH * 1.15),(int)(CUSTOMER_HEIGHT * 1.15),Image.SCALE_FAST);
		
		myController = controller;
		
		//Start/Pause Button
		myStartPauseButton = new JButton("START");
		myStartPauseButton.setFont(myFont3);
		
		;

		//Frame info
		this.setSize(1000,600);
		this.setLocation(0, 0);
		this.setTitle("Service Queues");
		this.setResizable(false);
		
		myContentPane = getContentPane();
		myContentPane.setLayout(new BorderLayout());
		
		//Sim Panel
		mySimPanel = new JPanel();
		mySimPanel.setBorder(BorderFactory.createLoweredBevelBorder());
		mySimPanel.setLayout(null);
		
		//Customer Served Counter
		myTotalServed = new JLabel[MAX_NUM_OF_TELLERS];
		
		for(int i = 0; i < myTotalServed.length; i++)
		{
			myTotalServed[i] = new JLabel(" 0");
			myTotalServed[i].setFont(myFont3);
			myTotalServed[i].setBackground(Color.WHITE);
			myTotalServed[i].setSize(COUNTER_BOX_WIDTH, COUNTER_BOX_HEIGHT);
			myTotalServed[i].setLocation(60+(CUSTOMER_WIDTH*i), 30);
			myTotalServed[i].setBorder(BorderFactory.createLoweredBevelBorder());
			mySimPanel.add(myTotalServed[i]);
		}

		//Teller locations
		myTeller = new JLabel[MAX_NUM_OF_TELLERS];
		myCounter = new JLabel[MAX_NUM_OF_TELLERS];
		
		myScaledCashier = new Image[MAX_NUM_OF_TELLERS];
		
		Image cashier1 = Toolkit.getDefaultToolkit().getImage(TELLER1_IMG);
		myScaledCashier[0] = cashier1.getScaledInstance(TELLER_WIDTH,TELLER_HEIGHT,Image.SCALE_FAST);
		Image cashier2 = Toolkit.getDefaultToolkit().getImage(TELLER2_IMG);
		myScaledCashier[1] = cashier2.getScaledInstance(TELLER_WIDTH,TELLER_HEIGHT,Image.SCALE_FAST);
		Image cashier3 = Toolkit.getDefaultToolkit().getImage(TELLER3_IMG);
		myScaledCashier[2] = cashier3.getScaledInstance(TELLER_WIDTH,TELLER_HEIGHT,Image.SCALE_FAST);
		Image cashier4 = Toolkit.getDefaultToolkit().getImage(TELLER4_IMG);
		myScaledCashier[3] = cashier4.getScaledInstance(TELLER_WIDTH,TELLER_HEIGHT,Image.SCALE_FAST);
		Image cashier5 = Toolkit.getDefaultToolkit().getImage(TELLER5_IMG);
		myScaledCashier[4] = cashier5.getScaledInstance(TELLER_WIDTH,TELLER_HEIGHT,Image.SCALE_FAST);
		
		for(int i = 0; i < MAX_NUM_OF_TELLERS; i++)
		{
			;
			myTeller[i] = new JLabel(new ImageIcon(myScaledCashier[i]));
			myTeller[i].setSize(TELLER_WIDTH, TELLER_HEIGHT);
			myTeller[i].setLocation(20+(CUSTOMER_WIDTH*i), ROW_1);
			myTeller[i].setVisible(true);
			mySimPanel.add(myTeller[i]);
			
			myCounter[i] = new JLabel(new ImageIcon(myScaledCounter));
			myCounter[i].setSize(TELLER_WIDTH, TELLER_HEIGHT);
			myCounter[i].setLocation((CUSTOMER_WIDTH*i)-10, ROW_1 + 10);
			myCounter[i].setVisible(true);
			mySimPanel.add(myCounter[i]);
		}
	
		//Customer Lines
		myCustomer = new JLabel[MAX_NUM_OF_TELLERS][MAX_PEOPLE_IN_LINE];
		for(int i = 0; i < MAX_NUM_OF_TELLERS; i++)
		{
			for(int j = 0; j < MAX_PEOPLE_IN_LINE; j++)
			{
				myCustomer[i][j] = new JLabel();
				myCustomer[i][j].setSize(CUSTOMER_WIDTH, CUSTOMER_HEIGHT);
				myCustomer[i][j].setLocation((CUSTOMER_WIDTH*i) - 50, 275 - (50*j));
				myCustomer[i][j].setVisible(true);
				mySimPanel.add(myCustomer[i][j]);
			}
		}
		
		//Background
		
		mySidebar = new JPanel(new GridLayout(2,1));
		mySidebar.setBorder(BorderFactory.createLoweredBevelBorder());
		myToolbar = new JPanel(new GridLayout(9,1));
		myToolbar.setSize(100, 500);
		
		
		JLabel bg;
		bg = new JLabel(new ImageIcon("images/background.png"));
		bg.setSize(500, 500);
		bg.setLocation(0, 0);
		mySimPanel.add(bg);
		
		
		myNumCashiersSlider.setMinorTickSpacing(1);
		myNumCashiersSlider.setPaintTicks(true);
		myNumCashiersSlider.setBorder(BorderFactory.createLoweredBevelBorder());
		
		myGenerateTimeName.setEditable(false);
		myNumCustomersName.setEditable(false);
		myNumCashiersName.setEditable(false);
		myServiceTimeName.setEditable(false);
		
		myGenerateTimeName.setFont(myFont2);
		myNumCustomersName.setFont(myFont2);
		myNumCashiersName.setFont(myFont2);
		myServiceTimeName.setFont(myFont2);
		
		myGenerateTimeName.setBorder(javax.swing.BorderFactory.createEmptyBorder());
		myNumCustomersName.setBorder(javax.swing.BorderFactory.createEmptyBorder());
		myNumCashiersName.setBorder(javax.swing.BorderFactory.createEmptyBorder());
		myServiceTimeName.setBorder(javax.swing.BorderFactory.createEmptyBorder());
		
		myGenerateTimeField.setBorder(BorderFactory.createLoweredBevelBorder());
		myNumCustomersField.setBorder(BorderFactory.createLoweredBevelBorder());
		myServiceTimeField.setBorder(BorderFactory.createLoweredBevelBorder());
		
		myToolbar.add(myGenerateTimeName);
		myToolbar.add(myGenerateTimeField);
		myToolbar.add(myNumCustomersName);
		myToolbar.add(myNumCustomersField);
		myToolbar.add(myNumCashiersName);
		myToolbar.add(myNumCashiersSlider);
		myToolbar.add(myServiceTimeName);
		myToolbar.add(myServiceTimeField);
		myToolbar.add(myStartPauseButton);
		
		myOverallDisplay = new JTextArea("OVERALL STATISTICS:");
		myOverallDisplay.setFont(myFont);
		myOverallDisplay.setEditable(false);
		myOverallDisplay.setBorder(BorderFactory.createLoweredBevelBorder());
		
		JPanel cashierPanel = new JPanel(new GridLayout(1,5));
		Dimension dimension = new Dimension(50,50);
		cashierPanel.setMinimumSize(dimension);
		myCashierDisplays = new JTextArea[MAX_NUM_OF_TELLERS];
		for (int i = 0; i < MAX_NUM_OF_TELLERS; i++)
		{
			myCashierDisplays[i] = new JTextArea("CASHIER STATISTICS: \n \n \n \n \n \n");
			myCashierDisplays[i].setFont(myFont2);
			myCashierDisplays[i].setEditable(false);
			myCashierDisplays[i].setBorder(BorderFactory.createLoweredBevelBorder());
			cashierPanel.add(myCashierDisplays[i]);
		}
		
		JPanel infoArea = new JPanel(new GridLayout());
		mySidebar.add(myToolbar);
		infoArea.add(myOverallDisplay);
		mySidebar.add(infoArea);
		myOverSimPanel = new JPanel(new BorderLayout());
		myOverSimPanel.add(mySimPanel, BorderLayout.CENTER);
		myOverSimPanel.add(cashierPanel, BorderLayout.SOUTH);
		myContentPane.add(myOverSimPanel, BorderLayout.CENTER);
		myContentPane.add(mySidebar, BorderLayout.EAST);
		
		
		
		
		this.associateListeners(myController);
		
		this.setVisible(true);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);	
	}
	
	//////////////////////////////////////////
	//            Methods                   //
	//////////////////////////////////////////

	/**
	 * Changes the button text from start to resume to pause depending on which is needed
	 * @author hudson
	 */
	public void changeStartPause() 
	{
		if(myStartPauseButton.getText().equals("PAUSE"))
		{
			myStartPauseButton.setText("RESUME");
		}
		else
		{
			if(myController.getModel() != null)
			{
				myStartPauseButton.setText("PAUSE");
			} else myStartPauseButton.setText("START");
		}
	}
	
	/**
	 * Takes in a queue and a number in line in order to display the customers in line
	 * @author hudson
	 * @param queue
	 * @param numInLine
	 */
	 public void setCustomersInLine(int queue, int numInLine)
	 {
		
		myTeller[queue].setIcon(new ImageIcon(myScaledCashier[queue]));
		
		for(int i = 0; i < MAX_PEOPLE_IN_LINE; i++)
		{
			myCustomer[queue][i].setVisible(false);
		}
		try
		{
			for(int i = 0; i < numInLine && i < MAX_PEOPLE_IN_LINE; i++)
			{
				myCustomer[queue][i].setVisible(true);
			    myCustomer[queue][i].setIcon(new ImageIcon(myScaledCustomer[0]));
			} 
		}
		catch (NullPointerException e)
		{
			
		}
	 }
	 
	/**
	 * Associates the button with the appropriate method
	 * @author hudson
	 * @param controller The controller in which the method is included.
	 */
	private void associateListeners(SimController controller)
	{
		Class<? extends SimController> controllerClass;
		Method startPauseMethod;
		
		controllerClass = myController.getClass();
				
		startPauseMethod = null;
		try 
		{
			startPauseMethod = 
				controllerClass.getMethod("startPause", (Class<?>[])null);
		} 
		catch (SecurityException e) 
		{	
			String error;

			error = e.toString();
			System.out.println(error);
		}  
		catch (NoSuchMethodException e) 
		{
			String error;

	        error = e.toString();
	        System.out.println(error);
		}


		myStartPauseListener = 
			new ButtonListener(myController, startPauseMethod, null);
				
		myStartPauseButton.addMouseListener(myStartPauseListener);
		
	}
	
	/**
	 * Returns the number of customers written in the accompanying text field for use in the model
	 * @author hudson
	 * @return
	 */
	public int getNumCustomers()
	{
		String word = myNumCustomersField.getText();
		if (word != null && word != "")
		{
			if (word.length() <= 0) return -1;
			for (int i = 0; i < word.length(); i++) 
			{
				if (!Character.isDigit(word.charAt(i)))
				{
					return -1;
				}
			}
		} else return -1;
		return Integer.parseInt(word);
	}
	
	/**
	 * Returns the time to generate per customer written in the accompanying text field for use in the model
	 * @author hudson
	 * @return
	 */
	public int getGenerateTime()
	{
		String word = myGenerateTimeField.getText();
		
		if (word != null)
		{
			if (word.length() <= 0) return -1;
			for (int i = 0; i < word.length(); i++) 
			{
				if (!Character.isDigit(word.charAt(i)))
				{
					return -1;
				}
			}
		} else return -1;
		
		
		return Integer.parseInt(word);
	}
	/**
	 * Returns the number of cashiers selected by the slider for use in the model
	 * @author hudson
	 * @return
	 */
	public int getNumCashiers()
	{
		return myNumCashiersSlider.getValue();
	}
	
	/**
	 * Gets the String from the service time field and converts it into an int for use 
	 * @author hudson
	 * @return
	 */
	public int getServiceTime()
	{
		String word = myServiceTimeField.getText();
		
		if (word != null)
		{
			if (word.length() <= 0) return -1;
			for (int i = 0; i < word.length(); i++) 
			{
				if (!Character.isDigit(word.charAt(i)))
				{
					return -1;
				}
			}
		} else return -1;
		
		
		return Integer.parseInt(word);
	}

	/**
	 * Resets View to initial text values
	 * @author hudson
	 */
	public void resetView()
	{
		myNumCashiersSlider.setValue(1);
		myServiceTimeField.setText(null);
		myNumCustomersField.setText(null);
		myGenerateTimeField.setText(null);
		myStartPauseButton.setText("Start");
		for (int i = 0; i < MAX_NUM_OF_TELLERS; i++)
		{
			myCashierDisplays[i].setText("CASHIER STATISTICS: \n \n \n \n \n \n");
		}
		
		myOverallDisplay.setText("OVERALL STATISTICS: ");
	}
	
	/**
	 * Updates the stats for both the overall and cashier statistic panels
	 * @author hudson
	 */
	public void updateStats()
	{
		myOverallDisplay.setText("OVERALL STATISTICS: ");
		myOverallDisplay.append("\nTotal Customers Served: \n" +  "  " +myController.getModel().getTotalServedSoFar());
		myOverallDisplay.append("\nTotal Service Time: \n" +  "  " +myController.getModel().getTotalServiceTime());
		myOverallDisplay.append("\nTotal Idle Time: \n" +  "  " +myController.getModel().getTotalIdleTime());
		myOverallDisplay.append("\nTotal Wait Time: \n" +  "  " +myController.getModel().getTotalWaitTime());
		myOverallDisplay.append("\nAverage Customers Served: \n" +  "  " +myController.getModel().getAverageServed());
		myOverallDisplay.append("\nTotal Time Elapsed: \n" +  "  " +myController.getModel().getTimeElapsed());
		myOverallDisplay.append("\nAverage Idle Time: \n" +  "  " +myController.getModel().getAverageIdleTime());
		myOverallDisplay.append("\nAverage Service Time: \n" +  "  " +myController.getModel().getAverageServiceTime());
		myOverallDisplay.append("\nAverage Wait Time: \n" +  "  " +myController.getModel().getAverageWaitTime());
		
		if (myController.getModel() != null)
		{
			for (int i = 0; i < myController.getModel().getNumberOfQueues(); i++)
			{
				int value = myController.getModel().getServiceQueue(i).getMyNumberCustomersInLine() - MAX_PEOPLE_IN_LINE;
				if (value > 0)
				{
					myTotalServed[i].setText("" + (value));
				} else myTotalServed[i].setText("0");
				
				myCashierDisplays[i].setText("CASHIER STATISTICS: ");
				myCashierDisplays[i].append("\nAverage Wait Time: \n" +  "  " +new DecimalFormat("#.00").format(myController.getModel().getCashier(i).getAverageWaitTime()));
				myCashierDisplays[i].append("\nAverage Service Time: \n" +  "  " +new DecimalFormat("#.00").format(myController.getModel().getCashier(i).getAverageServiceTime()));
				myCashierDisplays[i].append("\nIdle Time: \n" + "  " +new DecimalFormat("#.00").format(myController.getModel().getCashier(i).getMyTotalIdleTime()));
				
			}
		}
		
	
	}
}
