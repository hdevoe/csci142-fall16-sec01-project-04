package queueMVC;



import java.awt.MouseInfo;

import queueSim.ServiceQueueManager;

/**
 * Controls the interaction between view and the service queue manager
 * @author hudson
 */
public class SimController implements Runnable
{
	private ServiceQueueManager myModel;
	private SimView myView;
	private boolean mySuspended = true;
	private Thread myThread;
	
	/**
	 * Constructor
	 * @author hudson
	 */
	public SimController()
	{
		myView = new SimView(this);
		
		
		myThread = new Thread(this);
		mySuspended = false;
		
		this.start();
	}
	
	/**
	 * Calls on the view to display the customers of the given queue
	 * @author hudson
	 * @param queue
	 */
	private void displayCustomers(int queue)
	{
		int numInQueue = myModel.getServiceQueue(queue).getMyNumberCustomersInLine();

		myView.setCustomersInLine(queue, numInQueue);
	}
	
	/**
	 * Running this thread updates the view as well as allows most of the interaction to take place
	 * @author hudson
	 */
	public void run()
    {
    	try
    	{
    		synchronized(this)
    		{
    			this.updateView();
    		}
    	}
    	catch (InterruptedException e)
    	{
    		System.out.println("Thread suspended.");
    	}
    }
	
	/**
	 * Continuously updates and displays the running threads from the model (Service Queue Manager)
	 * @author hudson
	 * @throws InterruptedException
	 */
	private void updateView()throws InterruptedException
	{
		while(true)
		{
			this.waitWhileSuspended();
			try 
			{
				Thread.sleep(200);
				if(myModel.checkIfStillRunning())
				{
					
					for(int x = 0; x < myModel.getNumberOfQueues(); x++)
					{
						this.displayCustomers(x);
					}
					myModel.checkQueuesEmpty();
					myView.updateStats();
				} else
				{
					myModel.nullifyAll();
					myModel = null;
					myView.resetView();
					this.suspend();
				}
			}
			catch (InterruptedException e) 
			{
				e.printStackTrace();
			}
			
		}
	}
	
	/**
	 * Causes the thread to wait while the boolean mySuspended is true
	 * @author hudson
	 * @throws InterruptedException
	 */
	private synchronized void waitWhileSuspended() throws InterruptedException
    {
    	while (mySuspended)
    	{
    		this.wait();
    	}
    }
    
	/**
	 * Sets mySuspended to true as well as suspends all threads from the queue manager
	 * @author hudson
	 */
    public void suspend()
    {
    	mySuspended = true;
    	
    	if (myModel != null)
    	{
    		myModel.getCustomerGenerator().suspend();
    	
    		for (int i = 0; i < myModel.getNumberOfQueues(); i++)
    		{
	    		myModel.getCashier(i).suspend();
	    	}
    	}
    }
    
    /**
     * Starts  this thread
     * @author hudson
     */
    public void start()
    {
        try
        {
            myThread.start();
            mySuspended = true;
        }
        catch(IllegalThreadStateException e)
        {
            System.out.println("Thread already started");
        }
    }
    
    /**
     * Resumes this thread as well as all the thread in Service Queue Manager
     * @author hudson
     */
    public synchronized void resume()
    {
    	myModel.getCustomerGenerator().resume();
    	
    	for (int i = 0; i < myModel.getNumberOfQueues(); i++)
    	{
    		myModel.getCashier(i).resume();
    	}
    	
    	mySuspended = false;
    	this.notify();
    }
    
    /**
     * Pauses the threads when mySuspended = false, resumes the threads when it equals true
     * @author hudson
     */
    public void startPause()
    {
    	if (mySuspended && myModel == null)
    	{
    		if (myView.getNumCustomers() != -1 && myView.getGenerateTime() != -1 && myView.getServiceTime() != -1)
    		{	
    			myModel = new ServiceQueueManager(myView.getNumCashiers(), myView.getNumCustomers(), myView.getGenerateTime(),myView.getServiceTime());
    			this.resume();
    			myModel.start();
    		}
    	} else if (mySuspended && myModel != null)
    	{
    		
    		this.resume();
    	}
    	else if (!mySuspended && myModel != null)
    	{	
    		this.suspend(); 
    	}
    	myView.changeStartPause();
    }
    

    /**
     * returns the model
     * @return
     */
    public ServiceQueueManager getModel()
    {
    	return myModel;
    }

    /**
     * The main function of the controller, starts everything else
     * @author hudson
     * @param args
     */
	public static void main(String[] args)
	{
		new SimController();
	}
}
