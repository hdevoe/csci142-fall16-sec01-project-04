package queueSim;

import java.util.LinkedList;

/**
 * Wrapper for linked list that only allows insertion from one point and removal from the other
 * @author hudson
 *
 * @param <T>
 */
public class Queue<T>
{
	private LinkedList<T> myData;
		
	public Queue()
	{
		myData = new LinkedList<T>();
	}
		
	public boolean offer(T o)
	{
		return myData.offer(o);
	}
		
	public T poll()
	{
		return myData.poll();
	}
		
	public T peek()
	{
		return myData.peek();
	}
		
	public boolean empty()
	{
		return myData.isEmpty();
	}

}

