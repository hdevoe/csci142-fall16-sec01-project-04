package queueSim;

import java.util.Random;

/**
 * The customer class is to be generated and placed in a queue with a given service time and then run through and popped off the queue by a Cashier
 * @author hudson
 *
 */
public class Customer 
{
	private int myServiceTime;
	private int myEntryTime;
	private int myWaitTime;
	private int myCustomerType;
	
	/**
	 * Constructor
	 * @author hudson
	 */
	public Customer()
	{
		Random myRandom = new Random();
        myEntryTime = (int) System.currentTimeMillis();
        myCustomerType = myRandom.nextInt(7);
	}
	
	/**
	 * returns the random service time of a given customer
	 * @author hudson
	 * @return
	 */
	public int getServiceTime()
	{
		return myServiceTime;
	}
	
	/**
	 * Subtracts the customer's entry time from the current time, returning the amount of time the customer waited in line
	 * @author hudson
	 * @return
	 */
	public int getWaitTime()
	{
		myWaitTime = (int)System.currentTimeMillis() - myEntryTime;
		return myWaitTime;
	}
	
	public int getCustomerType()
	{
		return myCustomerType;
	}
}
