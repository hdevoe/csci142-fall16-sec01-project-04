package queueSim;

import java.util.Random;

/**
 * Generates the given number of customers at the rate given by the user
 * @author hudson
 *
 */
public class CustomerGenerator implements Runnable
{

	private int myMaxTimeBetweenCustomers;
	private Thread myThread;
	private ServiceQueueManager myServiceQueueManager;
	private int myNumberOfCustomers;
	private int myCounter = 1;
	
	private boolean mySuspended;
	private boolean myIsRunning;

	/**
	 * The constructor takes in a max time between customers, the number of customers, and the service queue manager it's a part of 
	 * @author hudson
	 * @param maxTimeBetweenCustomers
	 * @param numberOfCustomers
	 * @param serviceQueueManager
	 */
	public CustomerGenerator(int maxTimeBetweenCustomers, int numberOfCustomers, ServiceQueueManager serviceQueueManager)
	{
		myMaxTimeBetweenCustomers = (int)maxTimeBetweenCustomers;
		myServiceQueueManager = serviceQueueManager;
		myThread = new Thread(this);
		myNumberOfCustomers = (int)numberOfCustomers;
		
		
	}
	
	/**
	 *Generates a customer if it hasn't reached the max number of customers and then offers the customer to the service queue within the service queue
	 *manager with the shortest line, waiting a random amount of time within the bounds set by the player
	 *@author hudson
	 * @throws InterruptedException
	 */
	public void generateCustomer() throws InterruptedException
	{
		
		while(myCounter <= myNumberOfCustomers)
		{
			this.waitWhileSuspended();
			
			Customer myCustomer = new Customer();
			try
            {
				ServiceQueue<Customer> myServiceQueue = myServiceQueueManager.determineShortestQueue();
				myServiceQueue.offer(myCustomer);
				myServiceQueue.addCustomerToLine();
				Random myRandom = new Random();
                Thread.sleep(myRandom.nextInt(myMaxTimeBetweenCustomers));
            }
            catch(InterruptedException e)
            {
            	System.out.println("Customer generator suspended.");
            }
			myCounter++;
		}
		myIsRunning = false;
		myServiceQueueManager.setGeneratorOff();
	}
	
	/**
	 * Runs the thread, calling on the generate customer method
	 * @author hudson
	 */
	@Override
	public void run() 
	{
		try
		{
			synchronized(this)
			{
				this.generateCustomer();
			}
		} 
		catch (InterruptedException e)
		{
			System.out.println("Customer generator suspended.");
		}
		
	}
	
	/**
	 * Starts the thread
	 * @author hudson
	 */
	public void start()
	{
		try
        {
			myIsRunning = true;
			mySuspended = false;
            myThread.start();
        }
        catch(IllegalThreadStateException e)
        {
            System.out.println("Thread already started");
        }
	}
	
	/**
	 * Returns the is running boolean
	 * @author hudson
	 * @return
	 */
	public boolean getIsRunning()
	{
		return myIsRunning;
	}
	
	/**
	 * returns the maximum time between customers
	 * @author hudson
	 * @return
	 */
	public int getmaxTime()
	{
		return myMaxTimeBetweenCustomers;
	}

	/**
	 * Tells the generateCustomerMethod to wait if mySuspended = true
	 * @author hudson
	 * @throws InterruptedException
	 */
	private synchronized void waitWhileSuspended() throws InterruptedException
    {
    	while (mySuspended)
    	{
    		this.wait();
    		
    	}
    }
    
	/**
	 * Sets the suspend boolean to true
	 * @author hudson
	 */
    public void suspend()
    {
    	mySuspended = true;
    }
    

    /**
     * Resumes running the thread
     * @author hudson
     */
    public synchronized void resume()
    {
    	mySuspended = false;
    	this.notify();
    }
    

	

	
}
