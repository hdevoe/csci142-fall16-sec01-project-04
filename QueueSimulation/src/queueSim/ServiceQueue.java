package queueSim;

/**
 * A wrapper for the queue class made to cater towards the service queue project
 * All of the methods are fairly self-explanatory
 * @author hudson
 *
 */
public class ServiceQueue<T> extends Queue<T>
{
	private int myNumberCustomersServedSoFar;
	private int myNumberCustomersInLine;
	
	/**
	 * Constructor
	 * @author hudson
	 */
	public ServiceQueue()
	{
		myNumberCustomersServedSoFar = 0;
		myNumberCustomersInLine = 0;
	}
	
	public int getMyNumberCustomersServedSoFar() 
	{
		return myNumberCustomersServedSoFar;
	}

	public void addToCustomersServed()
	{
		myNumberCustomersServedSoFar += 1;
	}

	public void addCustomerToLine()
	{
		myNumberCustomersInLine += 1;
	}
	
	public void removeCustomerFromLine()
	{
		myNumberCustomersInLine -= 1;
	}
	
	
	public int getMyNumberCustomersInLine() {
		return myNumberCustomersInLine;
	}
}
