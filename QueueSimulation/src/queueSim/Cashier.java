package queueSim;

import java.util.Random;

/**
 * The cashier is created alongside the service queue that it peeks and polls from until it is empty
 * @author hudson
 *
 */
public class Cashier implements Runnable 
{

	private int myMaxTimeOfService;
	private ServiceQueue<Customer> myServiceQueue;
	private ServiceQueueManager myServiceQueueManager;
	private Thread myThread;
	private boolean mySuspended = false;
	private int myTotalWaitTime;
	private int myTotalServiceTime;
	private int myTotalIdleTime;
	private int myCustomersServed;
	
	/**
	 * Constructor take is max service time, the serviceQueue it is responsible for, and the ServiceQueueManager it was created and is 
	 * managed by
	 * @author hudson
	 * @param maxServiceTime
	 * @param serviceQueue
	 * @param myManager
	 */
	public Cashier(int maxServiceTime, ServiceQueue<Customer> serviceQueue, ServiceQueueManager myManager)
	{
		myMaxTimeOfService = maxServiceTime;
		myServiceQueue = serviceQueue;
		myServiceQueueManager = myManager;
		myThread = new Thread(this);
		myTotalWaitTime = 0;
		myTotalServiceTime = 0;
		myTotalIdleTime = 0;
		myCustomersServed = 0;
	}

	/**
	 * Waits a random amount of time within the given bounds, checks to see if the queue is empty,
	 * and, if it isn't, then it peeks and pops off the next customer in line
	 * @author hudson
	 * @throws InterruptedException
	 */
	public void serveCustomer() throws InterruptedException
	{
		while(true)
		{
			this.waitWhileSuspended();
			Random myRandom = new Random();
			int timeToServe = myRandom.nextInt(myMaxTimeOfService);
			try 
			{
				int timePassed = (int)System.currentTimeMillis();
				if (!myServiceQueue.empty())
				{
					myTotalWaitTime += myServiceQueue.peek().getWaitTime();
					int entryTime = (int)System.currentTimeMillis();
					Thread.sleep((long)timeToServe);
					myServiceQueue.poll();
					myServiceQueue.removeCustomerFromLine();
					myServiceQueueManager.incrementCustomers();
					myCustomersServed++;
					myTotalServiceTime += ((int)System.currentTimeMillis() - entryTime);
				} else myTotalIdleTime += ((int)System.currentTimeMillis() - timePassed);
			} catch (InterruptedException e) 
			{
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * Runs the given thread
	 * @author hudson
	 */
	@Override
	public void run() 
	{
		try
		{
			synchronized(this)
			{
				this.serveCustomer();
			}
		} 
		catch (InterruptedException e)
		{
			System.out.println("Customer generator suspended.");
		}
		
	}
	
	/**
	 * Starts the thread
	 * @author hudson
	 */
	public void start() 
	{
		try
        {
            myThread.start();
        }
        catch(IllegalThreadStateException e)
        {
            System.out.println("Thread already started");
        }
		
	}
	
	/**
	 * Resumes the thread and sets suspended to false
	 * @author hudson
	 */
    public synchronized void resume()
    {
    	mySuspended = false;
    	this.notify();
    }
    
    //self-explanatory
    public void suspend()
    {
    	mySuspended = true;
    }
    
    /**
     * Causes the thread to wait while mySuspended = true
     * @throws InterruptedException
     */
    private synchronized void waitWhileSuspended() throws InterruptedException
    {
    	while (mySuspended)
    	{
    		this.wait();
    		
    	}
    }
   
    /**
     * returns the average wait time for all customers served so far
     * @author hudson
     * @return
     */
	public double getAverageWaitTime()
	{
		if (myCustomersServed != 0)
		{
			return 1.0 * myTotalWaitTime / myCustomersServed;
		}
		return 0;
	}
	
	/**
     * returns the average service time for all customers served so far
     * @author hudson
     * @return
     */
	public double  getAverageServiceTime()
	{
		if (myCustomersServed != 0)
		{
			return 1.0 * myTotalServiceTime / myCustomersServed;
		}
		return 0;
	}

	public int getMyTotalWaitTime() {
		return myTotalWaitTime;
	}

	public int getMyTotalServiceTime() {
		return myTotalServiceTime;
	}

	public int getMyTotalIdleTime() {
		return myTotalIdleTime;
	}
	
	
}
