package queueSim;

import java.text.DecimalFormat;

/**
 * The service queue manager manages the service queues, the customer generators, and the cashiers, 
 * taking customers from the generator and placing them in the shortest available queue where they 
 * are popped off the queue by the cashiers
 * @author hudson
 */

public class ServiceQueueManager 
{

	public static final int MAX_NUMBER_OF_QUEUES = 5;
	private int myNumberOfServiceQueues;
	private CustomerGenerator myCustomerGenerator;
	private ServiceQueue<Customer>[] myServiceQueues;
	private Cashier[] myCashiers;
	
	private boolean myWorking;
	private boolean myGeneratorWorking;
	private boolean myQueuesFilled;
	private int myStartTime;
	
	private int myTotalServed;
	
	/**
	 * Constructor generates the service queues, cashiers for those service queues, and the customer generator all with the parameters given 
	 * @author hudson
	 * @param numberOfServiceQueues
	 * @param numberOfCustomers
	 * @param generationSpeed
	 * @param serviceTime
	 */
	@SuppressWarnings("unchecked")
	public ServiceQueueManager(int numberOfServiceQueues, int numberOfCustomers, int generationSpeed, int serviceTime)
	{
		myNumberOfServiceQueues = numberOfServiceQueues;
		myServiceQueues = new ServiceQueue[myNumberOfServiceQueues];
		myCashiers = new Cashier[myNumberOfServiceQueues];
		
		for (int i = 0; i < myNumberOfServiceQueues;i++)
		{
			myServiceQueues[i] = new ServiceQueue<Customer>();
			myCashiers[i] = new Cashier(serviceTime,myServiceQueues[i],this);
		}
		
		myCustomerGenerator = new CustomerGenerator(generationSpeed, numberOfCustomers, this);
		
		myGeneratorWorking = true;
		myQueuesFilled = true;
		myTotalServed = 0;
		myStartTime = (int)System.currentTimeMillis();
	}
	
	//self-explanatory
	public void incrementCustomers()
	{
		myTotalServed++;
	}
	
	//self-explanatory
	public int getTotalServedSoFar()
	{
		return myTotalServed;
	}
	
	/**
	 * This method is used by the customer generator to determine the shortest available queue and then add the generated customer to the
	 * end of it
	 * @author hudson
	 * @return
	 */
	public ServiceQueue<Customer> determineShortestQueue()
	{
		int shortest = myServiceQueues[0].getMyNumberCustomersInLine();
		int index = 0;
		for (int i = 0; i < myNumberOfServiceQueues; i++)
		{
			if (myServiceQueues[i].getMyNumberCustomersInLine() < shortest)
			{
				shortest = myServiceQueues[i].getMyNumberCustomersInLine();
				index = i;
			}
		}
		return myServiceQueues[index];
	}
	
	/**
	 * Returns the service queue with the given index
	 * @param queue
	 * @return
	 */
	public ServiceQueue<Customer> getServiceQueue(int queue)
	{
		return myServiceQueues[queue];
	}
	
	//self-explanatory
	public int getNumberOfQueues()
	{
		return myNumberOfServiceQueues;
	}
	
	//self-explanatory
	public CustomerGenerator getCustomerGenerator()
	{
		return myCustomerGenerator;
	}
	
	/**
	 * Starts the cashier and customer generator threads
	 * @author hudson
	 */
	public void start()
    {
        try
        {
        	myCustomerGenerator.start();
        	
            for (int i = 0; i < myNumberOfServiceQueues; i++)
            {
            	myCashiers[i].start();
            }
            myWorking = true;
        }
        catch(IllegalThreadStateException e)
        {
            System.out.println("Thread already started");
        }
    }
    
	/**
	 * Returns the cashier with the given index
	 * @author hudson
	 * @param index
	 * @return
	 */
	public Cashier getCashier(int index)
	{
		return myCashiers[index];
	}

	/**
	 * Used by the customer generator class to notify this class that it is no longer running, and if the service queues are empty then it sets
	 * myWorking to false
	 * @author hudson
	 */
	public void setGeneratorOff()
	{
		myGeneratorWorking = false;
		if (!myQueuesFilled)
		{
			myWorking = false;
		}
	}
	
	/**
	 * Checks to see if the queues are empty, and if the customer generator is also not running then myWorking is set to false
	 * @author hudson
	 * @return
	 */
	public boolean checkQueuesEmpty()
	{
		for (int i = 0; i < myNumberOfServiceQueues; i++)
        {
        	if (!myServiceQueues[i].empty())
        	{
        		myQueuesFilled = true;
        		return true;
        	}
        } 
		myQueuesFilled = false;
		this.setQueuesEmpty();
		return false;
	}
	
	/**
	 * Set Queues empty
	 * @author hudson
	 */
	public void setQueuesEmpty()
	{
		if(!myGeneratorWorking)
		{
			myWorking = false;
		}
	}
	
	//self-explanatory
	public boolean checkIfStillRunning()
	{		
		return myWorking;
	}
	
	/**
	 * Sets all queues, cashiers, and the generator to null
	 * @author hudson
	 */
	public void nullifyAll()
	{
		for (int i = 0; i < myNumberOfServiceQueues; i++)
        {
        	myServiceQueues[i] = null;
        	myCashiers[i] = null;
        } 
		myCustomerGenerator = null;
	}
	
	/**
	 * Returns the service time for all the cashiers
	 * @author hudson
	 * @return
	 */
	public int getTotalServiceTime()
	{
		int total = 0;
		for (int i = 0; i < myNumberOfServiceQueues; i++)
		{
			total += myCashiers[i].getMyTotalServiceTime();
		}
		return total;
	}
	
	/**
	 * Returns the idle time for all the cashiers
	 * @author hudson
	 * @return
	 */
	public int getTotalIdleTime()
	{
		int total = 0;
		for (int i = 0; i < myNumberOfServiceQueues; i++)
		{
			total += myCashiers[i].getMyTotalIdleTime();
		}
		return total;
	}
	
	/**
	 * Returns the wait time for all the cashiers
	 * @author hudson
	 * @return
	 */
	public int getTotalWaitTime()
	{
		int total = 0;
		for (int i = 0; i < myNumberOfServiceQueues; i++)
		{
			total += myCashiers[i].getMyTotalWaitTime();
		}
		return total;
	}
	
	/**
	 * Returns the average wait time for all the cashiers
	 * @author hudson
	 * @return
	 */
	public String getAverageWaitTime()
	{
		double total = 0.0;
		for (int i = 0; i < myNumberOfServiceQueues; i++)
		{
			total += myCashiers[i].getAverageWaitTime();
		}
		return new DecimalFormat("#.00").format(1.0 * total/myNumberOfServiceQueues);
	}
	
	/**
	 * Returns the average service time for all the cashiers
	 * @author hudson
	 * @return
	 */
	public String getAverageServiceTime()
	{
		double total = 0.0;
		
		for (int i = 0; i < myNumberOfServiceQueues; i++)
		{
			total += myCashiers[i].getAverageServiceTime();
		}
		return new DecimalFormat("#.00").format(1.0 * total/myNumberOfServiceQueues);
	}
	
	/**
	 * Returns the average idle time for all the cashiers
	 * @author hudson
	 * @return
	 */
	public String getAverageIdleTime()
	{
		return new DecimalFormat("#.00").format(1.0 * getTotalIdleTime()/myNumberOfServiceQueues);
	}
	
	/**
	 * Returns the average number of customers served for all the cashiers
	 * @author hudson
	 * @return
	 */
	public String getAverageServed()
	{
		return new DecimalFormat("#.00").format(1.0 * getTotalServedSoFar()/myNumberOfServiceQueues);
	}
	
	/**
	 * Returns the time that has elapsed since the ServiceQueueManager was created
	 * @author hudson
	 * @return
	 */
	public int getTimeElapsed()
	{
		return (int)System.currentTimeMillis() - myStartTime;
	}
}

